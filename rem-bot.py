import os
import asyncio
import random
import time
import sys

from datetime import datetime
from enum import Enum

import discord
import requests
import json
import urllib

from dotenv import load_dotenv
from discord.ext import commands, tasks
from urllib.parse import quote as urlquote
from kbbi import KBBI

# from discord.utils import get

load_dotenv()
bot_token =     os.getenv('rem_bot_token')
guild_id =      os.getenv('guild_id')
bot_id =        os.getenv('m_rem')
bot_thumbnail = os.getenv('rem_bot_thumbnail')
BOT_PREFIX =    os.getenv('rem_prefix')

# colors
color_rem_blue =    discord.Color(value=int(os.getenv('color_rem_blue'), 16))
color_ram_pink =    discord.Color(value=int(os.getenv('color_ram_pink'), 16))
color_red =         discord.Color(value=int(os.getenv('color_red'), 16))
color_orange =      discord.Color(value=int(os.getenv('color_orange'), 16))
color_yellow =      discord.Color(value=int(os.getenv('color_yellow'), 16))
color_green =       discord.Color(value=int(os.getenv('color_green'), 16))
color_mint =        discord.Color(value=int(os.getenv('color_mint'), 16))
color_purple =      discord.Color(value=int(os.getenv('color_purple'), 16))
color_white =       discord.Color(value=int(os.getenv('color_white'), 16))
color_black =       discord.Color(value=int(os.getenv('color_black'), 16))

bot = commands.Bot(command_prefix=BOT_PREFIX)
bot.remove_command('help')  # delete help standard discordpy

bot_reply_msg = []
admin_role_msg_id = None
delay_time = 10


# ==== bot login =======================================================================================================
@bot.event
async def on_ready():
    guild = bot.get_guild(int(guild_id))
    print(f"Rem Bot ONLINE! connected to {guild.name}: {str(guild.id)}")
    print(f"prefix : {BOT_PREFIX}")

    await bot.change_presence(
        activity=discord.Activity(type=discord.ActivityType.listening, name=f"commands. | {BOT_PREFIX}help"))


# ==== Welcome DM ======================================================================================================
@bot.event
async def on_member_join(member):
    guild = bot.get_guild(int(guild_id))

    # channels
    tc_welcome = guild.get_channel(int(os.getenv('tc_welcome')))
    
    # sends welcome message
    to_send =   f"{member.name}, welcome to {guild.name}!\n My name is Rem, and I'm the designated bot for this server!\n If you need any help, you can type {BOT_PREFIX}help in any text channel you can type in, or here in my DM. Or check on {tc_welcome.mention} for more info."
    dm = await member.create_dm()
    await dm.send(to_send)

    # sends rules
    embed = discord.Embed(
        title="Rules",
        description=f"- Don't be annoying\n"
                    f"- Use your common sense\n"
                    f"- Don't spam too much\n"
                    f"- Use the channels for its intended topic\n"
                    f"- Try to keep chat in english\n"
                    f"- No self advertising\n",
        color=color_rem_blue)
    dm = await member.create_dm()
    await dm.send(embed=embed)


# ==== change status ===================================================================================================
# @tasks.loop(seconds=30)
# async def status_manager():
#     await bot.wait_until_ready()
#
#     await bot.change_presence(
#         activity=discord.Activity(type=discord.ActivityType.listening, name=f"commands. {BOT_PREFIX}help"))
#

#    pantsu =
#
# if :   #at least 2 pants are in the same game
#     await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.playing,
#                                                         name="with the pantsu squad"))
# elif:  #__kneko is live
#     await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching,
#                                                         name="__kneko live on twitch"))
# elif:  #Jerryfishu is live
#     await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching,
#                                                         name="Jerryfishu live on twitch"))
# else:
#     await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.listening,
#                                                         name="commands. Tip: ??help"))
#
# random_status = random.randint(0, 3)
#
# # while not bot.is_closed():
# if random_status == 0:
#     await bot.change_presence(
#         activity=discord.Activity(type=discord.ActivityType.listening,
#                                   name="commands. Tip: {}help".format(BOT_PREFIX)))
# elif random_status == 1:
#     await bot.change_presence(
#         activity=discord.Activity(type=discord.ActivityType.playing, name="with the pantsu squad"))
# elif random_status == 2:
#     await bot.change_presence(
#         activity=discord.Activity(type=discord.ActivityType.watching, name="__kneko live on twitch"))
# elif random_status == 3:
#     await bot.change_presence(
#         activity=discord.Activity(type=discord.ActivityType.watching, name="Jerryfishu live on twitch"))


# ==== bot reply deleter ===============================================================================================
@tasks.loop(seconds=30)
async def reply_deleter():
    # delete reply after few seconds
    for i in bot_reply_msg:
        await asyncio.sleep(delay_time)
        await i.delete()


# ==== ??? =============================================================================================================
# @bot.event
# async def on_member_update(before, after):
#     activities = after.activities
#     for act in activities:
#         if isinstance(act, discord.Streaming):
#             print("stream")


# ==== !!!ADMIN!!! Server guide message ===============================================================================
@bot.command(name='admin_serverguide')
async def channel_guide(ctx):
    guild = bot.get_guild(int(guild_id))

    # channels
    tc_welcome =            guild.get_channel(int(os.getenv('tc_welcome')))
    tc_greetings =          guild.get_channel(int(os.getenv('tc_greetings')))
    tc_announcements =      guild.get_channel(int(os.getenv('tc_announcements')))
    tc_vc_help =            guild.get_channel(int(os.getenv('tc_vc_help')))
    tc_radio_dj =           guild.get_channel(int(os.getenv('tc_radio_dj')))
    tc_chat_en =            guild.get_channel(int(os.getenv('tc_chat_en')))
    tc_chat_ina =           guild.get_channel(int(os.getenv('tc_chat_ina')))
    tc_roles =              guild.get_channel(int(os.getenv('tc_roles')))
    tc_memes =              guild.get_channel(int(os.getenv('tc_memes')))
    tc_food =               guild.get_channel(int(os.getenv('tc_food')))
    tc_wiabus_strike =      guild.get_channel(int(os.getenv('tc_wiabus_strike')))
    tc_neko_doggo =         guild.get_channel(int(os.getenv('tc_neko_doggo')))
    tc_nsfw =               guild.get_channel(int(os.getenv('tc_nsfw')))
    tc_kneko_art =          guild.get_channel(int(os.getenv('tc_kneko_art')))
    tc_i_made_an_art =      guild.get_channel(int(os.getenv('tc_i_made_an_art')))
    tc_events =             guild.get_channel(int(os.getenv('tc_events')))
    tc_the_zoo =            guild.get_channel(int(os.getenv('tc_the_zoo')))
    tc_monster_hunter =     guild.get_channel(int(os.getenv('tc_monster_hunter')))
    tc_animal_crossing =    guild.get_channel(int(os.getenv('tc_animal_crossing')))
    tc_safetythird =        guild.get_channel(int(os.getenv('tc_safetythird')))
    tc_pantura =            guild.get_channel(int(os.getenv('tc_pantura')))
    tc_gtav =               guild.get_channel(int(os.getenv('tc_gtav')))
    tc_osu =                guild.get_channel(int(os.getenv('tc_osu')))
    tc_other_games =        guild.get_channel(int(os.getenv('tc_other_games')))
    tc_support =            guild.get_channel(int(os.getenv('tc_support')))
    tc_bot_command =        guild.get_channel(int(os.getenv('tc_bot_command')))

    # roles to mention
    r_OWNER =       guild.get_role(int(os.getenv('r_OWNER')))
    r_weeblet =     guild.get_role(int(os.getenv('r_weeblet')))
    r_pantsu =      guild.get_role(int(os.getenv('r_pantsu')))
    r_art =         guild.get_role(int(os.getenv('r_art')))
    r_animals =     guild.get_role(int(os.getenv('r_animals')))
    r_momon =       guild.get_role(int(os.getenv('r_momon')))
    r_hpcs =        guild.get_role(int(os.getenv('r_hpcs')))
    r_safetythird = guild.get_role(int(os.getenv('r_safetythird')))
    r_pantura =     guild.get_role(int(os.getenv('r_pantura')))
    r_acnh =        guild.get_role(int(os.getenv('r_acnh')))
    r_osu =         guild.get_role(int(os.getenv('r_osu')))
    
    # welcome message
    embed_welcome_message = discord.Embed(title=f"Welcome to {guild.name}!",
                            description="Thank you for joining our server!\n\n"
                                        "This server is the place where kneko and his friends usually hang out. Most of us here likes video games and art, and I hope you do too!\n"
                                        "\n"
                                        "Before you can access the whole channel, please make sure you read all the important information below! And don't forget to verify at the end!",
                            color=color_rem_blue)
    
    # rules
    embed_rules = discord.Embed(title="Rules",
                            description=f"1. Don't be annoying\n"
                                        f"2. Use your common sense\n"
                                        f"3. Don't spam too much\n"
                                        f"4. Use the channels for its intended topic\n"
                                        f"5. Try to keep chat in english\n"
                                        f"6. No self advertising\n",
                            color=color_red)
    embed_rules_play = discord.Embed(title='Unofficial "rules" when playing with us',
                            description="1. Fun comes first, win or lose doesn't matter\n"
                                        "2. Getting good doesn't necessarily means trying hard\n",
                            color=color_red)
    
    # channel guide
    embed_channel_guide_0 = discord.Embed(title="Channel guide",
                            description=f"{tc_welcome.mention}\n"
                                        f"All **important information** you need about this server can be found in this channel."
                                        f"\n\n"
                                        
                                        f"{tc_greetings.mention}\n"
                                        f"Greets all the **new people** who joins the server."
                                        f"\n\n"
                                        
                                        f"{tc_announcements.mention}\n"
                                        f"All kinds of **announcements** goes here. Livestreams, video uploads, PSA, and all other general announcements."
                                        f"\n\n",
                            color=color_green)
    embed_channel_guide_1 = discord.Embed(title="",
                            description=f"{tc_vc_help.mention}\n"
                                        f"**Voice management** bot commands only."
                                        f"\n\n"
                                        
                                        f"{tc_radio_dj.mention}\n"
                                        f"Music bot commands and ONLY for **music controls**."
                                        f"\n\n",
                            color=color_green)
    embed_channel_guide_2 = discord.Embed(title="",
                            description=f"{tc_chat_en.mention}\n"
                                        f"Chat room in **english**. Share anything you like, but remember "
                                        f"some topics already have their own channel. **NO NSFW CONTENTS!**"
                                        f"\n\n"
                                        
                                        f"{tc_chat_ina.mention}\n"
                                        f"Chat room in **indonesian**. If you can't fully understand english, or maybe if something are simply better to be informed in my native language. Share anything you like, but remember "
                                        f"some topics already have their own channel. **NO NSFW CONTENTS!**"
                                        f"\n\n"
                                        
                                        f"{tc_roles.mention}\n"
                                        f"For **self role assignment**."
                                        f"You don't need to ask owner/mods for role requests. React to get your chosen role, or see the example on how to assign your own role using command."
                                        f"\n\n"
                                        
                                        f"{tc_memes.mention}\n"
                                        f"Place where anyone can share **quality memes**."
                                        f"\n\n"
                                        
                                        f"{tc_food.mention}\n"
                                        f"Anything **cooking** or **food** related goes here. Don't open when you hungry. You've been warned."
                                        f"\n\n"
                                        
                                        f"{tc_wiabus_strike.mention}\n"
                                        f"**Weeb** stuff goes here."
                                        f"\n\n"
                                        
                                        f"{tc_neko_doggo.mention}\n"
                                        f"Everyone likes **cats** and **dogs**, right? If you find some cute pictures (or drawings too) of them or maybe you have pets, you can share them here."
                                        f"\n\n"
                                        
                                        f"{tc_nsfw.mention}\n"
                                        f"The **NSFW** channel. Not necessarily rule-less, use your common sense. Don't share anything too extreme. Enter at your own risk."
                                        f"\n\n",
                            color=color_green)
    embed_channel_guide_3 = discord.Embed(title="",
                            description=f"{tc_kneko_art.mention}\n"
                                        f"I will post **my art** here once I finished it!^^"
                                        f"\n\n"
                                        
                                        f"{tc_i_made_an_art.mention}\n"
                                        f"Channel to share **your art**. Doesn't matter if it's good or bad, everybody starts somewhere. But try to finish it before sharing here.^^"
                                        f"\n\n",
                            color=color_green)
    embed_channel_guide_4 = discord.Embed(title="",
                            description=f"{tc_events.mention}\n"
                                        f"Create, enter and manage **events** or **LFG (looking for group)**."
                                        f"\n\n"
                                        
                                        f"{tc_the_zoo.mention}\n"
                                        f"All **CS:GO** related stuff goes in this channel."
                                        f"\n\n"
                                        
                                        f"{tc_monster_hunter.mention}\n"
                                        f"All **Monster Hunter** related stuff goes in this channel."
                                        f"\n\n"
                                        
                                        f"{tc_animal_crossing.mention}\n"
                                        f"All **Animal Crossing** related stuff goes in this channel."
                                        f"\n\n"
                                        
                                        f"{tc_safetythird.mention}\n"
                                        f"All **Sim-racing/drifting** related stuff goes in this channel."
                                        f"\n\n"
                                        
                                        f"{tc_pantura.mention}\n"
                                        f"All **Euro/American Truck Simulator** related stuff goes in this channel."
                                        f"\n\n"
                                        
                                        f"{tc_gtav.mention}\n"
                                        f"All **GTA Online** related stuff goes in this channel."
                                        f"\n\n"
                                        
                                        f"{tc_osu.mention}\n"
                                        f"All **OSU!** related stuff goes in this channel."
                                        f"\n\n"
                                        
                                        f"{tc_other_games.mention}\n"
                                        f"Stuff from **other games not in the list** goes in this channel."
                                        f"\n\n",
                            color=color_green)
    embed_channel_guide_5 = discord.Embed(title="",
                            description=f"{tc_support.mention}\n"
                                        f"Tell us if you have any **problems**, **suggestions** or **requests** here."
                                        f"\n\n"
                                        
                                        f"{tc_bot_command.mention}\n"
                                        f"Only for **bot commands**. Every registered member can type in this channel. If you think you'll make a mess trying out bot commands in the other channels, try them here instead!"
                                        f"\n\n",
                            color=color_green)
    embed_channel_guide_5.set_footer(text=f"Type {BOT_PREFIX}help in any channel you can type in or in my DM if you need help ^^",
                        icon_url=bot_thumbnail)
    
    # roles
    embed_roles = discord.Embed(title="Roles",
                            description=f"{r_OWNER.mention} : Well, it's only for me, kneko ^^\n"
                                        f"{r_pantsu.mention} : The admins. This is mostly for my IRL friends, or anyone who I know really close\n"
                                        f"{r_weeblet.mention} : For all of you who understand and accept the rules in this server\n"
                                        "\n"
                                        f"We also have roles for several games! They are used when you want to look for group to play or if you just interested in any of these games.\n"
                                        "\n"
                                        f"Currently we have roles for :\n"
                                        f"- {r_art.mention} - Art\n"
                                        f"- {r_animals.mention} - CS:GO\n"
                                        f"- {r_momon.mention} - Monster Hunter World\n"
                                        f"- {r_acnh.mention} - Animal Crossing: New Horizon\n"
                                        f"- {r_safetythird.mention} - Sim racing/drifting\n"
                                        f"- {r_pantura.mention} - Euro/American Truck Simulator\n"
                                        f"- {r_hpcs.mention} - GTA V Online\n"
                                        f"- {r_osu.mention} - Osu!\n"
                                        "\n"
                                        f"Let me know if I should add more game roles!\n"
                                        f"You can assign yourself to any of these game roles by visiting {tc_roles.mention}!\n",
                            color=color_ram_pink)
        
    # important links
    embed_links = discord.Embed(title="Important links",
                            description="Twitch channel | https://www.twitch.tv/kneko__\n"
                                        "YouTube channel (art) | https://www.youtube.com/channel/UCHvajgrcuBuVot7CTN4eXkQ\n"
                                        "YouTube channel (game) | https://www.youtube.com/channel/UCMFgin4pRuV4jGfLEU3vjog\n"
                                        "Instagram | https://www.instagram.com/__kneko/\n"
                                        "Twitter | https://twitter.com/__kneko\n"
                                        "\n"
                                        "This server's permanent invite link | https://discord.gg/UrxsdGr\n",
                            color=color_yellow)
    
    # delete invoking command
    await ctx.message.delete()
    
    # display embeds
    await ctx.send(embed=embed_welcome_message)
    await ctx.send(embed=embed_rules)
    await ctx.send(embed=embed_rules_play)
    await ctx.send(embed=embed_channel_guide_0)
    await ctx.send(embed=embed_channel_guide_1)
    await ctx.send(embed=embed_channel_guide_2)
    await ctx.send(embed=embed_channel_guide_3)
    await ctx.send(embed=embed_channel_guide_4)
    await ctx.send(embed=embed_channel_guide_5)
    await ctx.send(embed=embed_roles)
    await ctx.send(embed=embed_links)


# ==== !!!ADMIN!!! Verification ========================================================================================
# todo : persis kek mee6 punya reaction role
#           copy admin_roles aja kalo dah beres, prinsip sama cuma 1 role doang
# @bot.command(name='admin_verification')
# async def verification(ctx):

# ==== !!!ADMIN!!! self assignable role (reaction) =====================================================================
# todo : bikin supaya walaupun bot nya reset, dia masi bisa react sama reaction role nya
# async def on_message(message):
#     if message.content.startswith('$thumb'):
#         channel = message.channel
#         await channel.send('Send me that 👍 reaction, mate')
#
#         def check(reaction, user):
#             return user == message.author and str(reaction.emoji) == '👍'
#
#         try:
#             reaction, user = await client.wait_for('reaction_add', timeout=60.0, check=check)
#         except asyncio.TimeoutError:
#             await channel.send('👎')
#         else:
#             await channel.send('👍')

@bot.command(name='admin_roles')
async def admin_roles(ctx):
    reactions = ["🎨", "🔫", "🐉", "🏝️", "🏎️", "🚚", "💸", "⭕"]

    guild = bot.get_guild(int(guild_id))

    # roles to mention
    r_art =         guild.get_role(int(os.getenv('r_art')))
    r_animals =     guild.get_role(int(os.getenv('r_animals')))
    r_momon =       guild.get_role(int(os.getenv('r_momon')))
    r_hpcs =        guild.get_role(int(os.getenv('r_hpcs')))
    r_safetythird = guild.get_role(int(os.getenv('r_safetythird')))
    r_pantura =     guild.get_role(int(os.getenv('r_pantura')))
    r_acnh =        guild.get_role(int(os.getenv('r_acnh')))
    r_osu =         guild.get_role(int(os.getenv('r_osu')))

    embed = discord.Embed(title="Available Roles",
                            description=f"🎨 {r_art.mention} - Art\n"
                                        f"🔫 {r_animals.mention} - CS:GO\n"
                                        f"🐉 {r_momon.mention} - Monster Hunter World\n"
                                        f"🏝️ {r_acnh.mention} - Animal Crossing: New Horizon\n"
                                        f"🏎️ {r_safetythird.mention} - Sim-racing/drifting\n"
                                        f"🚚 {r_pantura.mention} - American/Euro Truck Simulator\n"
                                        f"💸 {r_hpcs.mention} - GTA V Online\n"
                                        f"⭕ {r_osu.mention} - Osu!\n",
                            color=color_rem_blue)
    embed.set_footer(text="React to the roles you wish to join below!")

    # delete invoking command
    await ctx.message.delete()

    # print message & add reactions
    role_msg = await ctx.send(embed=embed)
    for emoji in reactions:
        await role_msg.add_reaction(emoji)

    # wait user reaction
    # !musti pake task nanti
    while True:
        reaction, user = await bot.wait_for('reaction_add')

        if (user.id != bot.user.id) and (role_msg.id == reaction.message.id):
            requested_role = None

            if user.id != bot.user.id:
                await reaction.remove(user)

            if reaction.emoji == "🔫":
                requested_role = r_animals
            elif reaction.emoji == "🐉":
                requested_role = r_momon
            elif reaction.emoji == "💸":
                requested_role = r_hpcs
            elif reaction.emoji == "🏎️":
                requested_role = r_safetythird
            elif reaction.emoji == "🚚":
                requested_role = r_pantura
            elif reaction.emoji == "🏝️":
                requested_role = r_acnh
            elif reaction.emoji == "⭕":
                requested_role = r_osu

            # if already have role       = remove role
            if requested_role in user.roles:
                await user.remove_roles(requested_role, reason="requested role removed")
                embed = discord.Embed(title=None,
                                        description=f"👋 {user.mention} just left {requested_role.mention}!",
                                        color=color_red)
            # if dont have the role yet  = add role
            else:
                await user.add_roles(requested_role, reason="requested role assigned")
                embed = discord.Embed(title=None,
                                        description=f"🎊🎉 {user.mention} just joined {requested_role.mention}!\n",
                                        color=color_rem_blue)

            bot_reply_msg.append(await ctx.send(embed=embed))


# ==== Bot Info ========================================================================================================
@bot.command(name='rem')
async def bot_bio(ctx):
    embed = discord.Embed(title="Who am i?", color=color_rem_blue)
    embed.set_thumbnail(url=bot_thumbnail)
    embed.add_field(name="Kana", value="レム")
    embed.add_field(name="Romaji", value="Rem")
    embed.add_field(name="Race", value="Oni")
    embed.add_field(name="Gender", value="Female")
    embed.add_field(name="Age", value="17")
    embed.add_field(name="Affiliation", value="Kingdom of Lugnica")

    # send
    await ctx.send(embed=embed)


# ==== Help ============================================================================================================
@bot.command(name='help', pass_context=True)
async def help_rem(ctx):
    num_of_page = 6

    # get message sender
    sender_id = ctx.message.author.id
    guild = bot.get_guild(int(guild_id))
    member = guild.get_member(sender_id)

    # bot embed page 1
    embed_p1 = discord.Embed(title=f"Help centre [page 1/{num_of_page}]",
                                description=None,
                                color=color_rem_blue)
    embed_p1.set_thumbnail(url=bot_thumbnail)
    embed_p1.add_field(name="Page content",
                        value=  "**1.** This page\n"
                                "**2.** List of **general** commands\n"
                                "**3.** List of **music control** commands\n"
                                "**4.** List of **role** commands\n"
                                "**5.** List of **voice channel** commands\n"
                                "**6.** List of **fun** commands")
    embed_p1.add_field(name="Navigation",
                        value=  "◀️ previous page\n"
                                "▶️ next page\n"
                                "⏮️ go to first page\n"
                                "⏭️ go to last page\n"
                                "⏹️ close the help page")

    # bot embed page 2
    embed_p2 = discord.Embed(title=f"General commands [page 2/{num_of_page}]",
                                description=None,
                                color=color_purple)
    embed_p2.set_thumbnail(url=bot_thumbnail)
    embed_p2.add_field(name=f"{BOT_PREFIX}help",
                        value="Show this help centre",
                        inline=False)
    embed_p2.add_field(name=f"{BOT_PREFIX}rem",
                        value="Who is rem?",
                        inline=False)
    # embed_p2.add_field(name="??ram", value="Where ram is and what she's doing right now", inline=False)
    embed_p2.add_field(name=f"{BOT_PREFIX}event",
                        value="Create a new event",
                        inline=False)

    # bot embed page 3
    embed_p3 = discord.Embed(title=f"Music control commands [page 3/{num_of_page}]",
                                description=None,
                                color=color_purple)
    embed_p3.set_thumbnail(url=bot_thumbnail)
    embed_p3.add_field(name="⚠️ UNDER CONSTRUCTION ⚠️",
                        value="*This features will come in future update!*",
                        inline=False)
    embed_p3.add_field(name=f"{BOT_PREFIX}play <song/youtube url>",
                        value="Play the selected song",
                        inline=False)
    embed_p3.add_field(name=f"{BOT_PREFIX}queue",
                        value="Show the play queue",
                        inline=False)
    embed_p3.add_field(name=f"{BOT_PREFIX}add <song/youtube url>",
                        value="Adds the song to the queue",
                        inline=False)
    embed_p3.add_field(name=f"{BOT_PREFIX}remove <number, number, ...>",
                        value="Remove selected song(s) from the queue",
                        inline=False)
    embed_p3.add_field(name=f"{BOT_PREFIX}purge",
                        value="Empty the queue",
                        inline=False)
    embed_p3.add_field(name=f"{BOT_PREFIX}leave",
                        value="Empty the queue and disconnect from the voice channel",
                        inline=False)

    # bot embed page 4
    # roles to mention
    r_art =         guild.get_role(int(os.getenv('r_art')))
    r_animals =     guild.get_role(int(os.getenv('r_animals')))
    r_momon =       guild.get_role(int(os.getenv('r_momon')))
    r_hpcs =        guild.get_role(int(os.getenv('r_hpcs')))
    r_safetythird = guild.get_role(int(os.getenv('r_safetythird')))
    r_pantura =     guild.get_role(int(os.getenv('r_pantura')))
    r_acnh =        guild.get_role(int(os.getenv('r_acnh')))
    r_osu =         guild.get_role(int(os.getenv('r_osu')))

    embed_p4 = discord.Embed(title=f"Role commands [page 4/{num_of_page}]",
                                description="In this channel we have self-assignable roles. If you wish to join one of these roles, simply follow the example below!",
                                color=color_purple)
    embed_p4.set_thumbnail(url=bot_thumbnail)
    embed_p4.add_field(name="Self-assignable roles",
                        value=  f"- {r_art.mention} - Art\n"
                                f"- {r_animals.mention} - CS:GO\n"
                                f"- {r_momon.mention} - Monster Hunter World\n"
                                f"- {r_acnh.mention} - Animal Crossing: New Horizon\n"
                                f"- {r_safetythird.mention} - Sim racing/drifting\n"
                                f"- {r_pantura.mention} - Euro/American Truck Simulator\n"
                                f"- {r_hpcs.mention} - GTA V Online\n"
                                f"- {r_osu.mention} - Osu!\n",
                        inline=False)
    embed_p4.add_field(name=f"{BOT_PREFIX}roles",
                        value="Shows all the self-assignable roles. You can then assign or leave a role simply with a reaction!",
                        inline=False)
    embed_p4.add_field(name=f"{BOT_PREFIX}<role name>",
                        value=  f"You can also assign or leave one of the roles above using command.\n"
                                f"**example**: `{BOT_PREFIX}art` will assign you to {r_art.mention}",
                        inline=False)

    # bot embed page 5
    embed_p5 = discord.Embed(title=f"Voice channel commands [page 5/{num_of_page}]",
                                description=None,
                                color=color_purple)
    embed_p5.set_thumbnail(url=bot_thumbnail)
    embed_p5.add_field(name="⚠️ UNDER CONSTRUCTION ⚠️",
                        value="*This features will come in the future update!*",
                        inline=False)
    embed_p5.add_field(name=f"{BOT_PREFIX}setup",
                        value="=== ADMIN ONLY ===",
                        inline=False)
    embed_p5.add_field(name=f"{BOT_PREFIX}lock",
                        value="Lock your voice channel",
                        inline=False)
    embed_p5.add_field(name=f"{BOT_PREFIX}unlock",
                        value="Unlock your voice channel",
                        inline=False)
    embed_p5.add_field(name=f"{BOT_PREFIX}name <name>",
                        value="Change the name of your voice channel for this session",
                        inline=False)
    embed_p5.add_field(name=f"{BOT_PREFIX}limit <number 1-99>",
                        value="Limit the number of participant of your voice channel",
                        inline=False)
    embed_p5.add_field(name=f"{BOT_PREFIX}claim",
                        value="Claim current voice channel",
                        inline=False)

    # bot embed page 6
    embed_p6 = discord.Embed(title=f"Fun commands [page 6/{num_of_page}]",
                                description=None,
                                color=color_purple)
    embed_p6.set_thumbnail(url=bot_thumbnail)
    embed_p6.add_field(name=f"{BOT_PREFIX}pick",
                        value="Randomly picks a user",
                        inline=False)
    embed_p6.add_field(name=f"{BOT_PREFIX}slap @<user>",
                        value=f"Slaps a user\n"
                                f"**example**: `{BOT_PREFIX}slap @{member.name}`",
                        inline=False)
    embed_p6.add_field(name=f"{BOT_PREFIX}urban <word> *or* {BOT_PREFIX}ud <word>",
                        value=f"Search urban dictionary",
                        inline=False)
    embed_p6.add_field(name=f"{BOT_PREFIX}roll <number of dice> <number of sides>",
                        value=f"Roll a dice\n"
                                f"**example**: `{BOT_PREFIX}roll 2 6` will roll 2 dice with 6 sides each",
                        inline=False)

    # send to user
    msg = await ctx.send(embed=embed_p1)
    help_page = 1

    # add reaction to message
    await msg.add_reaction(emoji='⏮️')
    await msg.add_reaction(emoji='◀️')
    await msg.add_reaction(emoji='▶️')
    await msg.add_reaction(emoji='⏭️')
    await msg.add_reaction(emoji='⏹️')

    # get sent message id
    msg_id = msg.id

    # function to know which page to show
    def show_page(help_page):
        if help_page == 1:
            return embed_p1
        elif help_page == 2:
            return embed_p2
        elif help_page == 3:
            return embed_p3
        elif help_page == 4:
            return embed_p4
        elif help_page == 5:
            return embed_p5
        elif help_page == 6:
            return embed_p6

    # wait user to react
    while True:
        try:
            reaction, user = await bot.wait_for('reaction_add', timeout=60.0)
            if reaction is None:
                await msg.clear_reactions()
            elif (sender_id == user.id) and (msg_id == reaction.message.id):
                if reaction.emoji == '⏮️':
                    if help_page > 1:
                        help_page = 1
                        await msg.edit(embed=show_page(help_page))
                elif reaction.emoji == '◀️':
                    if help_page > 1:
                        help_page -= 1
                    elif help_page == 1:
                        help_page = num_of_page
                    await msg.edit(embed=show_page(help_page))
                elif reaction.emoji == '▶️':
                    if help_page < num_of_page:
                        help_page += 1
                    elif help_page == num_of_page:
                        help_page = 1
                    await msg.edit(embed=show_page(help_page))
                elif reaction.emoji == '⏭️':
                    if help_page < num_of_page:
                        help_page = num_of_page
                        await msg.edit(embed=show_page(help_page))
                elif reaction.emoji == '⏹️':
                    await msg.delete()
                    await ctx.message.delete()
                    break

                if user.id != bot.user.id:
                    await reaction.remove(user)
        except asyncio.TimeoutError:
            await msg.clear_reactions()
            await msg.delete()
            await ctx.message.delete()

            # respond too long message
            too_long_msg = await ctx.send(f"It took you too long to respond, {member.mention}. You can try again anytime!")
            time.sleep(delay_time)
            await too_long_msg.delete()

            break


# ==== self assignable role (command) ==================================================================================
@bot.command(aliases=['art', 'animals', 'momon', 'hpcs', 'safetythird', 'pantura', 'acnh', 'osu'])
async def role(ctx):
    guild = bot.get_guild(int(guild_id))
    sender_id = ctx.message.author.id
    msg_author = guild.get_member(sender_id)

    invalid_format = False
    requested_role = None

    if f"{BOT_PREFIX}art" == ctx.message.content:
        requested_role = guild.get_role(int(os.getenv('r_art')))
    elif f"{BOT_PREFIX}animals" == ctx.message.content:
        requested_role = guild.get_role(int(os.getenv('r_animals')))
    elif f"{BOT_PREFIX}momon" == ctx.message.content:
        requested_role = guild.get_role(int(os.getenv('r_momon')))
    elif f"{BOT_PREFIX}hpcs" == ctx.message.content:
        requested_role = guild.get_role(int(os.getenv('r_hpcs')))
    elif f"{BOT_PREFIX}safetythird" == ctx.message.content:
        requested_role = guild.get_role(int(os.getenv('r_safetythird')))
    elif f"{BOT_PREFIX}pantura" == ctx.message.content:
        requested_role = guild.get_role(int(os.getenv('r_pantura')))
    elif f"{BOT_PREFIX}acnh" == ctx.message.content:
        requested_role = guild.get_role(int(os.getenv('r_acnh')))
    elif f"{BOT_PREFIX}osu" == ctx.message.content:
        requested_role = guild.get_role(int(os.getenv('r_osu')))
    elif f"{BOT_PREFIX}role" == ctx.message.content:
        invalid_format = True
        bot_reply_msg.append(await ctx.send(f"💢 **That format is invalid!**\n"
                                            f"Try something like this: `{BOT_PREFIX}<role name>` "
                                            f"without the brackets.\n"
                                            f"Example: `{BOT_PREFIX}osu`"))

    if not invalid_format:
        # if already have role       = remove role
        if requested_role in msg_author.roles:
            await msg_author.remove_roles(requested_role, reason="requested role removed")
            embed = discord.Embed(title="👋",
                                    description=f"{msg_author.mention} just left {requested_role.mention}!",
                                    color=color_red)
        # if dont have the role yet  = add role
        else:
            await msg_author.add_roles(requested_role, reason="requested role assigned")
            embed = discord.Embed(title="🎊🎉",
                                    description=f"{msg_author.mention} just joined {requested_role.mention}!\n",
                                    color=color_rem_blue)

        bot_reply_msg.append(await ctx.send(embed=embed))

        # dm mentioning requester in corresponding channel

    await ctx.message.delete()


# ==== self assignable role (reaction) =================================================================================
@bot.command(name='roles')
async def roles(ctx):
    reactions = ["🎨", "🔫", "🐉", "🏝️", "🏎️", "🚚", "💸", "⭕"]

    guild = bot.get_guild(int(guild_id))
    sender_id = ctx.message.author.id
    member = guild.get_member(sender_id)

    # roles to mention
    r_art =         guild.get_role(int(os.getenv('r_art')))
    r_animals =     guild.get_role(int(os.getenv('r_animals')))
    r_momon =       guild.get_role(int(os.getenv('r_momon')))
    r_hpcs =        guild.get_role(int(os.getenv('r_hpcs')))
    r_safetythird = guild.get_role(int(os.getenv('r_safetythird')))
    r_pantura =     guild.get_role(int(os.getenv('r_pantura')))
    r_acnh =        guild.get_role(int(os.getenv('r_acnh')))
    r_osu =         guild.get_role(int(os.getenv('r_osu')))

    embed = discord.Embed(title="Available Roles",
                            description=f"🎨 {r_art.mention} - Art\n"
                                        f"🔫 {r_animals.mention} - CS:GO\n"
                                        f"🐉 {r_momon.mention} - Monster Hunter World\n"
                                        f"🏝️ {r_acnh.mention} - Animal Crossing: New Horizon\n"
                                        f"🏎️ {r_safetythird.mention} - Sim-racing/drifting\n"
                                        f"🚚 {r_pantura.mention} - American/Euro Truck Simulator\n"
                                        f"💸 {r_hpcs.mention} - GTA V Online\n"
                                        f"⭕ {r_osu.mention} - Osu!\n",
                            color=color_rem_blue)
    embed.set_footer(text="React to the roles you wish to join below!")

    # delete invoking command
    await ctx.message.delete()

    # print message & add reactions
    role_msg = await ctx.send(embed=embed)
    role_msg_id = role_msg.id
    for emoji in reactions:
        await role_msg.add_reaction(emoji)

    # wait user reaction
    while True:
        try:
            reaction, user = await bot.wait_for('reaction_add', timeout=30.0)
            if reaction is None:
                await role_msg.clear_reactions()
            elif (sender_id == user.id) and (role_msg_id == reaction.message.id):
                requested_role = None

                if user.id != bot.user.id:
                    await reaction.remove(user)

                if reaction.emoji == "🔫":
                    requested_role = r_animals
                elif reaction.emoji == "🐉":
                    requested_role = r_momon
                elif reaction.emoji == "💸":
                    requested_role = r_hpcs
                elif reaction.emoji == "🏎️":
                    requested_role = r_safetythird
                elif reaction.emoji == "🚚":
                    requested_role = r_pantura
                elif reaction.emoji == "🏝️":
                    requested_role = r_acnh
                elif reaction.emoji == "⭕":
                    requested_role = r_osu

                # if already have role       = remove role
                if requested_role in user.roles:
                    await member.remove_roles(requested_role, reason="requested role removed")
                    embed = discord.Embed(title=None,
                                            description=f"👋 {member.mention} just left {requested_role.mention}!",
                                            color=color_red)
                # if dont have the role yet  = add role
                else:
                    await member.add_roles(requested_role, reason="requested role assigned")
                    embed = discord.Embed(title=None,
                                            description=f"🎊🎉 {member.mention} just joined {requested_role.mention}!\n",
                                            color=color_rem_blue)

                bot_reply_msg.append(await ctx.send(embed=embed))

        except asyncio.TimeoutError:
            await role_msg.clear_reactions()
            await role_msg.delete()

            # respond too long message
            too_long_msg = await ctx.send(
                f"It took you too long to respond, {member.mention}. You can try again anytime!")
            time.sleep(delay_time)
            await too_long_msg.delete()

            break


# ==== roll dice =======================================================================================================
@bot.command(name='roll', help='Simulates rolling dice.')
async def roll_dice(ctx, number_of_dice: int, number_of_sides: int):
    dice = [
        str(random.choice(range(1, number_of_sides + 1)))
        for _ in range(number_of_dice)
    ]
    await ctx.send(', '.join(dice))


# ==== slap =======================================================================================================
# todo : need no arg error handler
tenorApiKey = "CMA9RE6PG08D"
limit = 1
search_term = "anime slap"


@bot.command(name='slap')
async def slap(ctx, arg):
    gif_url = None
    bad_arg = True

    guild = bot.get_guild(int(guild_id))
    sender_id = ctx.message.author.id
    req = requests.get("https://api.tenor.com/v1/random?q=%s&key=%s&limit=%s" % (search_term, tenorApiKey, limit))

    if req.status_code == 200:
        slap_gif = json.loads(req.content)
        gif_url = slap_gif["results"][0]["media"][0]["mediumgif"]["url"]
    else:
        req_error_msg = await ctx.send(f"💢 Bad argument, {ctx.message.author.mention}! I slap u instead!")
        time.sleep(delay_time)
        await req_error_msg.delete()
        pass

    for i in range(len(guild.members)):
        # if member is in channel
        if str(guild.members[i].id) == str(''.join(i for i in arg if i.isdigit())):
            bad_arg = False

            # if slap self
            if str(''.join(i for i in arg if i.isdigit())) == str(sender_id):
                desc = f"lol ok, {ctx.message.author.mention}"
                embed_color = color_orange
            # if slaps the bot
            elif str(''.join(i for i in arg if i.isdigit())) == str(bot_id):
                await ctx.send(f"💢 why you do that, {ctx.message.author.mention}?!")
                break
            else:
                desc = f"{ctx.message.author.mention} slaps {arg}"
                embed_color = color_rem_blue


            gif_res = gif_url

            embed = discord.Embed(title=None, description=desc, color=embed_color)
            embed.set_image(url=gif_res)

            await ctx.send(embed=embed)
            break

    if bad_arg:
        await ctx.send(f"💢 Bad argument, {ctx.message.author.mention}! I slap u instead!")
        gif_res = gif_url
        embed = discord.Embed(title=None, description=None, color=color_red)
        embed.set_image(url=gif_res)
        await ctx.send(embed=embed)


# ==== pick random =====================================================================================================
@bot.command(name='pick')
async def pick_random(ctx):
    # change nickname
    guild = bot.get_guild(int(guild_id))
    me = guild.me
    await me.edit(nick="rem is picking...")

    # get message sender
    sender_id = ctx.message.author.id
    guild = bot.get_guild(int(guild_id))
    member = guild.get_member(sender_id)

    reactions = ["📥", "📤", "✅", "🗑️"]
    participants = []

    embed = discord.Embed(title="Random picker",
                            description="📥 Join\n"
                                        "📤 Withdraw\n"
                                        "✅ **Pick!**\n"
                                        "🗑️    Cancel",
                            color=color_rem_blue)
    embed.add_field(name="Participants", value="-")
    embed.set_footer(text=f"Created by {member.name}")

    # print message & add reactions
    pick_msg = await ctx.send(embed=embed)

    # add reaction to message
    await pick_msg.add_reaction(emoji="📥")
    await pick_msg.add_reaction(emoji="📤")
    await pick_msg.add_reaction(emoji="✅")
    await pick_msg.add_reaction(emoji="🗑️")

    # delete invoking command
    await ctx.message.delete()

    # display reactions
    for emoji in reactions:
        await pick_msg.add_reaction(emoji)

    # waiting for reaction
    while True:
        try:
            reaction, user = await bot.wait_for('reaction_add', timeout=30.0)

            if reaction is not None and (user.id != bot.user.id) and (pick_msg.id == reaction.message.id):

                if reaction.emoji == "📥":  # add to participant pool
                    await reaction.remove(user)

                    if user.id not in participants:
                        participants.append(user.id)
                    else:
                        await ctx.send(f"you were already participating, {user.mention}")

                    # edit embed to show the participants
                    p_list = []
                    for p in participants:
                        p_list_singles = guild.get_member(p)
                        p_list.append(p_list_singles.name)

                    embed.set_field_at(0, name="Participants", value='\n'.join(p_list), inline=False)
                    await pick_msg.edit(embed=embed)  # Send updated embed

                elif reaction.emoji == "📤":  # remove from participant pool
                    await reaction.remove(user)

                    if user.id in participants:
                        participants.remove(user.id)
                    else:
                        await ctx.send(f"you were not participating, {user.mention}")

                    # edit embed to show the participants
                    p_list = []
                    for p in participants:
                        p_list_singles = guild.get_member(p)
                        p_list.append(p_list_singles.name)

                    if not participants:
                        embed.set_field_at(0, name="Participants", value="-")
                        await pick_msg.edit(embed=embed)  # Send updated embed
                    else:
                        embed.set_field_at(0, name="Participants", value='\n'.join(p_list), inline=False)
                        await pick_msg.edit(embed=embed)  # Send updated embed

                elif reaction.emoji == "✅":  # pick a random participant
                    await reaction.remove(user)

                    if not participants:
                        await ctx.send("Nobody is participating :(")
                    else:
                        await pick_msg.clear_reactions()
                        await pick_msg.delete()

                        picked = random.choice(participants)
                        # picked = random.sample(participants, k=1)  # k = how many to choose
                        picked_user = guild.get_member(picked)
                        await ctx.send(f"I picked {picked_user.mention}!")

                    # change nick back
                    guild = bot.get_guild(int(guild_id))
                    me = guild.me
                    await me.edit(nick="rem")

                    break

                elif reaction.emoji == "🗑️":  # cancel picking, delete message, delete reactions
                    await pick_msg.clear_reactions()
                    await pick_msg.delete()

                    # change nickname back
                    guild = bot.get_guild(int(guild_id))
                    me = guild.me
                    await me.edit(nick="rem")

                    break

        except asyncio.TimeoutError:
            # await pick_msg.clear_reactions()
            await pick_msg.delete()

            # respond too long message
            too_long_msg = await ctx.send(
                "It took you too long to respond. You can try again anytime!")
            time.sleep(delay_time)
            await too_long_msg.delete()

            # change nickname back
            guild = bot.get_guild(int(guild_id))
            me = guild.me
            await me.edit(nick="rem")

            break


# ==== event manager ===================================================================================================
# todo :
#   !!! kalo ga salah ada masalah sama timeout
#   cancel reaction in every step
#   msg event cancelled ilang abis 1 jam
#   reminder 15 mins sebelom event. DM ke semua yg accepted. yg join 14 menit sebelom mulai ga dapet reminder
#   delete semua emote kecuali trashbin pas event mulai
#   delete event embed 1 jam setelah event mulai

@bot.command(name='event')
async def event_manager(ctx):
    timeout_time = 60
    max_title_char = 200
    max_desc_char = 1600
    max_participants = 50
    # max_supported_tz = 2

    invoked_warning = False

    guild = bot.get_guild(int(guild_id))
    sender_id = ctx.message.author.id
    event_author = guild.get_member(sender_id)

    # roles to mention
    r_pantsu =      guild.get_role(int(os.getenv('r_pantsu')))
    r_weeblet =     guild.get_role(int(os.getenv('r_weeblet')))
    r_animals =     guild.get_role(int(os.getenv('r_animals')))
    r_momon =       guild.get_role(int(os.getenv('r_momon')))
    r_hpcs =        guild.get_role(int(os.getenv('r_hpcs')))
    r_safetythird = guild.get_role(int(os.getenv('r_safetythird')))
    r_pantura =     guild.get_role(int(os.getenv('r_pantura')))
    r_acnh =        guild.get_role(int(os.getenv('r_acnh')))
    r_osu =         guild.get_role(int(os.getenv('r_osu')))

    event_reaction = ["✅", "❌", "❔", "🗑️"]
    event_roles_reaction = ["🩲", "🧸", "🔫", "🐉", "💸", "🏎️", "🚚", "🏝️", "⭕", "🆓"]

    event_p_accept = []
    event_p_decline = []
    event_p_unsure = []

    try:
        await ctx.message.delete()
        msg_creating_event = await ctx.send(f"{event_author.mention} is creating an event...")

        warning_msg = None

        # ===== step 1 - title
        ask_embed = discord.Embed(title="Enter the event title",
                                    description=f"Type `-` for no title. Up to {max_title_char} characters are permitted",
                                    color=color_yellow)
        ask_msg = await ctx.send(embed=ask_embed)

        while True:
            # wait for input
            msg = await bot.wait_for('message', timeout=timeout_time)

            if msg.author.id == event_author.id:
                if int(len(msg.content)) > max_title_char:
                    if invoked_warning:
                        await warning_msg.delete()
                    await msg.delete()

                    warning_msg = await ctx.send(f"The **title** is too long. Please, try again.")
                    invoked_warning = True
                    continue

                if str(msg.content) == "-":
                    event_title = None
                else:
                    event_title = str(msg.content)

                if invoked_warning:
                    await warning_msg.delete()
                    invoked_warning = False
                await msg.delete()
                await ask_msg.delete()

                break
            else:
                continue

        # ===== step 2 - desc
        ask_embed = discord.Embed(title="Enter the event description",
                                    description=f"Type `-` for no description. Up to {max_desc_char} "
                                                f"characters are permitted.",
                                    color=color_yellow)
        ask_msg = await ctx.send(embed=ask_embed)

        while True:
            # wait for input
            msg = await bot.wait_for('message', timeout=timeout_time)

            if msg.author.id == event_author.id:
                if int(len(msg.content)) > max_desc_char:
                    if invoked_warning:
                        await warning_msg.delete()
                    await msg.delete()

                    warning_msg = await ctx.send(f"The **description** is too long. Please, try again.")
                    invoked_warning = True
                    continue

                if str(msg.content) == "-":
                    event_desc = None
                else:
                    event_desc = str(msg.content)

                if invoked_warning:
                    await warning_msg.delete()
                    invoked_warning = False
                await msg.delete()
                await ask_msg.delete()

                break
            else:
                continue

        # ===== step 3 - max participants
        ask_embed = discord.Embed(title="Enter the maximum number of participants",
                                    description=f"Up to {max_participants} "
                                                f"participants are permitted.",
                                    color=color_yellow)
        ask_msg = await ctx.send(embed=ask_embed)

        while True:
            # wait for input
            msg = await bot.wait_for('message', timeout=timeout_time)

            if msg.author.id == event_author.id:
                # if not int
                if not msg.content.isdigit():
                    if invoked_warning:
                        await warning_msg.delete()
                    await msg.delete()

                    warning_msg = await ctx.send(f"That's not a number. Please, try again.")
                    invoked_warning = True
                    continue

                if int(msg.content) > max_participants:
                    if invoked_warning:
                        await warning_msg.delete()
                    await msg.delete()

                    warning_msg = await ctx.send(f"I can't handle that many participants. "
                                                    f"Please, try again with a lower number.")
                    invoked_warning = True
                    continue
                elif int(msg.content) == 0:
                    if invoked_warning:
                        await warning_msg.delete()
                    await msg.delete()

                    warning_msg = await ctx.send(f"Maximum participants can't be 0. "
                                                    f"Please, try again with a higher number.")
                    invoked_warning = True
                    continue

                event_max_participants = int(msg.content)

                if invoked_warning:
                    await warning_msg.delete()
                    invoked_warning = False
                await msg.delete()
                await ask_msg.delete()

                break
            else:
                continue

        # ===== step 4 - timezone
        # ask_embed = discord.Embed(title="Enter your time zone's number",
        #                             description="**1** Central Europe\n"
        #                                         "**2** Indochina\n"
        #                                         "\n*more time zones will coming!*"
        #                                         "\n\n**For now just type in the time zone name. for example: `CEST`**",
        #                             color=color_yellow)
        ask_embed = discord.Embed(title="Enter your time zone",
                                    description="Type in the time zone name. for example: `CEST`",
                                    color=color_yellow)
        ask_msg = await ctx.send(embed=ask_embed)

        while True:
            # wait for input
            msg = await bot.wait_for('message', timeout=timeout_time)

            if msg.author.id == event_author.id:
                # # if not int
                # if not msg.content.isdigit():
                #     if invoked_warning:
                #         await warning_msg.delete()
                #     await msg.delete()
                #     await ask_msg.delete()
                #
                #     warning_msg = await ctx.send(f"That's not a number. Please, try again.")
                #     invoked_warning = True
                #     continue
                #
                # if int(msg.content) > max_supported_tz or int(msg.content) == 0:
                #     if invoked_warning:
                #         await warning_msg.delete()
                #     await msg.delete()
                #
                #     warning_msg = await ctx.send(f"That time zone is not supported yet. "
                #                                  f"Please, try again with a lower number.")
                #     invoked_warning = True
                #     continue
                #
                # # todo : pake pytz later
                # event_timezone = str(int(msg.content))
                event_timezone = str(msg.content)

                # if invoked_warning:
                #     await warning_msg.delete()
                #     invoked_warning = False
                await msg.delete()
                await ask_msg.delete()

                break
            else:
                continue

        # ===== step 5 - time
        # todo : bikin handler buat
        #  now
        #  in 1/an hour
        #  in <x> minutes
        #  in <x> hours
        #  in <x> days
        #  next month
        #  in <x> months
        #  next year
        #  <day> at <x>am/pm
        #  tomorrow at <x>am/pm
        #  dd-mm-yyyy hh:mm am/pm

        # ask_embed = discord.Embed(title="When will the event start?",
        #                             description="> friday at 9pm\n"
        #                                         "> tomorrow at 18:00\n"
        #                                         "> now\n"
        #                                         "> in 1 hour\n"
        #                                         "> dd-mm-yyyy 7:00 pm\n"
        #                                         "\n\n**Simply type in the time. examples: `7:30 pm`**",
        #                             color=color_yellow)
        ask_embed = discord.Embed(title="When will the event start?",
                                    description="**Simply type in the time.** examples:\n"
                                                "> 7:30 pm\n"
                                                "> friday at 9 pm\n"
                                                "> tomorrow at 18:00\n"
                                                "> dd-mm-yyyy 7:00 pm",
                                    color=color_yellow)
        ask_msg = await ctx.send(embed=ask_embed)

        while True:
            # wait for input
            msg = await bot.wait_for('message', timeout=timeout_time)

            if msg.author.id == event_author.id:
                # todo :
                #  save as time
                #  pake time interpreter later
                event_time = str(msg.content)

                # if invoked_warning:
                #     await warning_msg.delete()
                #     invoked_warning = False
                await msg.delete()
                await ask_msg.delete()

                break
            else:
                continue

        # ===== step 6 - mention
        ask_embed = discord.Embed(title="Do you want to mention any role?",
                                    description=f"Mentionable roles:\n"
                                                f"🩲 {r_pantsu.mention} - pantsu squad\n"
                                                f"🧸 {r_weeblet.mention} - weeblets\n"
                                                f"🔫 {r_animals.mention} - CS:GO/Overwatch\n"
                                                f"🐉 {r_momon.mention} - Monster Hunter\n"
                                                f"💸 {r_hpcs.mention} - GTA online\n"
                                                f"🏎️ {r_safetythird.mention} - Sim-racing/drifting\n"
                                                f"🚚 {r_pantura.mention} - American/Euro Truck Simulator\n"
                                                f"🏝️ {r_acnh.mention} - Animal Crossing\n"
                                                f"⭕ {r_osu.mention} - OSU!\n"
                                                f"🆓 **anyone can join** (don't mention anyone)\n",
                                    color=color_yellow)
        ask_msg = await ctx.send(embed=ask_embed)

        # display the reactions
        for emoji in event_roles_reaction:
            await ask_msg.add_reaction(emoji)

        while True:
            reaction, user = await bot.wait_for('reaction_add', timeout=timeout_time)
            if reaction is None:
                break
            elif (event_author.id == user.id) and (ask_msg.id == reaction.message.id):
                if reaction.emoji == "🩲":
                    event_mention = r_pantsu
                    break
                elif reaction.emoji == "🧸":
                    event_mention = r_weeblet
                    break
                elif reaction.emoji == "🔫":
                    event_mention = r_animals
                    break
                elif reaction.emoji == "🐉":
                    event_mention = r_momon
                    break
                elif reaction.emoji == "💸":
                    event_mention = r_hpcs
                    break
                elif reaction.emoji == "🏎️":
                    event_mention = r_safetythird
                    break
                elif reaction.emoji == "🚚":
                    event_mention = r_pantura
                    break
                elif reaction.emoji == "🏝️":
                    event_mention = r_acnh
                    break
                elif reaction.emoji == "⭕":
                    event_mention = r_osu
                    break
                elif reaction.emoji == "🆓":
                    event_mention = None
                    break

        await ask_msg.clear_reactions()
        await ask_msg.delete()
        await msg_creating_event.delete()

        # ===== step 7 - displaying created event
        # mention the chosen role
        if event_mention:
            created_event_mention_msg = await ctx.send(
                f"🔻✨ **There's a new event for {event_mention.mention} members!** ✨🔻\n"
                f"React to the corresponding emoji to let me know if you are "
                f"participating or not! <:rem5:681192685863960578>")
        else:
            created_event_mention_msg = await ctx.send(
                f"🔻✨ **There's a new event for anyone to participate!** ✨🔻\n"
                f"React to the corresponding emoji to let me know if you are "
                f"participating or not! <:rem5:681192685863960578>")

        # display embed
        created_event_embed = discord.Embed(title=event_title,
                                            description=event_desc,
                                            color=color_rem_blue)
        created_event_embed.set_thumbnail(url=bot_thumbnail)
        created_event_embed.set_footer(text=f"Created by {event_author.name}\n"
                                            f"{event_timezone} • {datetime.now()}")
        created_event_embed.add_field(name="Time", value=f"{event_time}", inline=False)
        created_event_embed.add_field(name=f"✅ Accepted ({len(event_p_accept)}/{event_max_participants})",
                                        value="-")
        created_event_embed.add_field(name=f"❌ Declined",
                                        value="-")
        created_event_embed.add_field(name=f"❔ Not sure",
                                        value="-")

        created_event_msg = await ctx.send(embed=created_event_embed)

        # display the reactions
        for emoji in event_reaction:
            await created_event_msg.add_reaction(emoji)

        # todo : later save as json
        print(f"mention message id: {created_event_mention_msg.id}")
        print(f"event embed id: {created_event_msg.id}")
        print(f"event author id: {event_author.id}")

        # ===== step 8 - handle reactions
        while True:
            reaction, user = await bot.wait_for('reaction_add')

            if reaction is not None and (user.id != bot.user.id) and (created_event_msg.id == reaction.message.id):

                if reaction.emoji == "✅":
                    await reaction.remove(user)

                    if user.id not in event_p_accept:
                        if user.id in event_p_decline:
                            event_p_decline.remove(user.id)

                            # edit embed to show the participants
                            pd_list = []
                            for pd in event_p_decline:
                                pd_list_singles = guild.get_member(pd)
                                pd_list.append(pd_list_singles.name)

                            if not event_p_decline:
                                created_event_embed.set_field_at(2, name=f"❌ Declined",
                                                                    value='-')
                            else:
                                created_event_embed.set_field_at(2, name=f"❌ Declined",
                                                                    value='\n'.join(pd_list))
                        if user.id in event_p_unsure:
                            event_p_unsure.remove(user.id)

                            # edit embed to show the participants
                            pu_list = []
                            for pu in event_p_unsure:
                                pu_list_singles = guild.get_member(pu)
                                pu_list.append(pu_list_singles.name)

                            if not event_p_unsure:
                                created_event_embed.set_field_at(3, name=f"❔ Not sure",
                                                                    value='-')
                            else:
                                created_event_embed.set_field_at(3, name=f"❔ Not sure",
                                                                    value='\n'.join(pu_list))
                        event_p_accept.append(user.id)
                    elif user.id in event_p_accept:
                        event_p_accept.remove(user.id)

                    # edit embed to show the participants
                    pa_list = []
                    for pa in event_p_accept:
                        pa_list_singles = guild.get_member(pa)
                        pa_list.append(pa_list_singles.name)

                    if not event_p_accept:
                        created_event_embed.set_field_at(1, name=f"✅ Accepted "
                                                                    f"({len(event_p_accept)}/{event_max_participants})",
                                                            value='-')
                    else:
                        created_event_embed.set_field_at(1, name=f"✅ Accepted "
                                                                    f"({len(event_p_accept)}/{event_max_participants})",
                                                            value='\n'.join(pa_list))
                    await created_event_msg.edit(embed=created_event_embed)

                elif reaction.emoji == "❌":
                    await reaction.remove(user)

                    if user.id not in event_p_decline:
                        if user.id in event_p_accept:
                            event_p_accept.remove(user.id)

                            # edit embed to show the participants
                            pa_list = []
                            for pa in event_p_accept:
                                pa_list_singles = guild.get_member(pa)
                                pa_list.append(pa_list_singles.name)

                            if not event_p_accept:
                                created_event_embed.set_field_at(1, name=f"✅ Accepted "
                                                                            f"({len(event_p_accept)}/"
                                                                            f"{event_max_participants})",
                                                                    value='-')
                            else:
                                created_event_embed.set_field_at(1, name=f"✅ Accepted "
                                                                            f"({len(event_p_accept)}/"
                                                                            f"{event_max_participants})",
                                                                    value='\n'.join(pa_list))
                        if user.id in event_p_unsure:
                            event_p_unsure.remove(user.id)

                            # edit embed to show the participants
                            pu_list = []
                            for pu in event_p_unsure:
                                pu_list_singles = guild.get_member(pu)
                                pu_list.append(pu_list_singles.name)

                            if not event_p_unsure:
                                created_event_embed.set_field_at(3, name=f"❔ Not sure",
                                                                    value='-')
                            else:
                                created_event_embed.set_field_at(3, name=f"❔ Not sure",
                                                                    value='\n'.join(pu_list))
                        event_p_decline.append(user.id)
                    elif user.id in event_p_decline:
                        event_p_decline.remove(user.id)

                    # edit embed to show the participants
                    pd_list = []
                    for pd in event_p_decline:
                        pd_list_singles = guild.get_member(pd)
                        pd_list.append(pd_list_singles.name)

                    if not event_p_decline:
                        created_event_embed.set_field_at(2, name=f"❌ Declined",
                                                            value='-')
                    else:
                        created_event_embed.set_field_at(2, name=f"❌ Declined",
                                                            value='\n'.join(pd_list))
                    await created_event_msg.edit(embed=created_event_embed)

                elif reaction.emoji == "❔":
                    await reaction.remove(user)

                    if user.id not in event_p_unsure:
                        if user.id in event_p_accept:
                            event_p_accept.remove(user.id)

                            # edit embed to show the participants
                            pa_list = []
                            for pa in event_p_accept:
                                pa_list_singles = guild.get_member(pa)
                                pa_list.append(pa_list_singles.name)

                            if not event_p_accept:
                                created_event_embed.set_field_at(1, name=f"✅ Accepted "
                                                                            f"({len(event_p_accept)}/"
                                                                            f"{event_max_participants})",
                                                                    value='-')
                            else:
                                created_event_embed.set_field_at(1, name=f"✅ Accepted "
                                                                            f"({len(event_p_accept)}/"
                                                                            f"{event_max_participants})",
                                                                    value='\n'.join(pa_list))
                        if user.id in event_p_decline:
                            event_p_decline.remove(user.id)

                            # edit embed to show the participants
                            pd_list = []
                            for pd in event_p_decline:
                                pd_list_singles = guild.get_member(pd)
                                pd_list.append(pd_list_singles.name)

                            if not event_p_decline:
                                created_event_embed.set_field_at(2, name=f"❌ Declined",
                                                                    value='-')
                            else:
                                created_event_embed.set_field_at(2, name=f"❌ Declined",
                                                                    value='\n'.join(pd_list))
                        event_p_unsure.append(user.id)
                    elif user.id in event_p_unsure:
                        event_p_unsure.remove(user.id)

                    # edit embed to show the participants
                    pu_list = []
                    for pu in event_p_unsure:
                        pu_list_singles = guild.get_member(pu)
                        pu_list.append(pu_list_singles.name)

                    if not event_p_unsure:
                        created_event_embed.set_field_at(3, name=f"❔ Not sure",
                                                            value='-')
                    else:
                        created_event_embed.set_field_at(3, name=f"❔ Not sure",
                                                            value='\n'.join(pu_list))
                    await created_event_msg.edit(embed=created_event_embed)

                elif reaction.emoji == "🗑️":
                    if user.id == event_author.id:
                        await created_event_msg.clear_reactions()
                        await created_event_msg.delete()
                        await created_event_mention_msg.delete()
                        break
                    else:
                        continue

        # ===== step 9 - call reminder

        # ===== step 10 - call event deleter

    except asyncio.TimeoutError:
        await msg_creating_event.delete()
        await ask_msg.clear_reactions()
        await ask_msg.delete()

        # respond too long message
        too_long_msg = await ctx.send(
            "It took you too long to respond. You can try again anytime!")
        time.sleep(delay_time)
        await too_long_msg.delete()


# @bot.event
# async def event_reminder(arg):
#     time_to_remind = arg


# @bot.event
# async def event_deleter(arg):
#     time_to_delete = arg


# ==== activity check ==================================================================================================
# todo :
#  cari cara supaya bisa baca activity type yg playing doang
#  trus save id nya

@bot.command(name='lapo')
async def lapo(ctx):
    __kneko_activities = []

    __kneko_id = os.getenv("m_KNEKO")
    guild = bot.get_guild(int(guild_id))
    __kneko = guild.get_member(int(__kneko_id))

    print(f"length = {len(__kneko.activities)}")
    print(__kneko.activities[0:len(__kneko.activities) - 1])

    i = 0
    tl = len(__kneko.activities)
    while i < tl:
        __kneko_activities.append(__kneko.activities[i])
        i = i + 1

    n = 0
    ll = len(__kneko_activities)
    while n < ll:
        await ctx.send(f"{__kneko_activities[n]}\n")
        n = n + 1


# ==== Urban Dictionary ================================================================================================
@bot.command(aliases=['urban', 'ud'])
async def urban_dictionary(ctx, *, content):
    url = "https://api.urbandictionary.com/v0/define?term=" + urlquote(content)
    sender_id = ctx.message.author.id
    delay_time = 60

    if content == '*':
        url = "https://api.urbandictionary.com/v0/random"
        raw_data = urllib.request.urlopen(url)
        data = json.loads(raw_data.read().decode())
        definitions = data['list']
    else:
        # fetch data
        raw_data = urllib.request.urlopen(url)
        data = json.loads(raw_data.read().decode())
        definitions = data['list']

    if len(definitions) == 0:
        not_found = discord.Embed(title=content.capitalize(), url="https://youtu.be/dQw4w9WgXcQ?t=43")
        await ctx.send(embed=not_found)
    else:
        embeds = []
        i = 1
        embeds_length = len(definitions)
        for deff in definitions:
            # prepare strings
            title = f"{str(deff['word'])}: {i}/{embeds_length}"
            def_with_link = analyzeText(deff['definition']).capitalize()
            vote = f"👍 {str(deff['thumbs_up'])}  👎 { str(deff['thumbs_down'])}"
            
            # creating Embed
            embed = discord.Embed(title=title, description=def_with_link, color=color_orange, url=deff['permalink'])
            # check example
            if len(deff['example']) > 0:
                example = analyzeText(deff['example']).capitalize()
                embed.add_field(name="Example(s)", value=example, inline=False)

            # add field
            embed.add_field(name="Votes", value=vote, inline=False)
            embed.set_footer(text=deff['author'])
            embeds.append(embed)
            i += 1

        # send message & add reactions
        page = 0
        if embeds_length == 1:
            await ctx.send(embed=embeds[page])
        else:
            msg = await ctx.send(embed=embeds[page])
            msg_id = msg.id
            await msg.add_reaction(emoji='◀️')
            await msg.add_reaction(emoji='▶️')
            await msg.add_reaction(emoji='⏹️')

            while True:
                try:
                    reaction, user = await bot.wait_for('reaction_add', timeout=delay_time)
                    if reaction is None:
                        await msg.clear_reactions()
                    elif (sender_id == user.id) and (msg_id == reaction.message.id):
                        if reaction.emoji == '◀️':
                            if page > 0:
                                page -= 1
                            elif page == 0:
                                page = embeds_length-1
                            await msg.edit(embed=embeds[page])
                        elif reaction.emoji == '▶️':
                            if page < embeds_length-1:
                                page += 1
                            elif page == embeds_length-1:
                                page = 0
                            await msg.edit(embed=embeds[page])
                        elif reaction.emoji == '⏹️':
                            await msg.clear_reactions()
                            break

                        if user.id != bot.user.id:
                            await reaction.remove(user)
                except asyncio.TimeoutError:
                    await msg.clear_reactions()
                    break

def analyzeText(text):
    url = "https://www.urbandictionary.com/define.php?term="
    ret = ""
    word = ""
    bracket = False
    for c in text:
        if c == '[':
            bracket = True
        elif bracket and not c == ']':
            word += c
        elif bracket and c == ']':
            bracket = False
            word = f"[{word}]({url + urlquote(word)})"
            ret = ret + word
            word = ""
        else:
            ret += c

    return ret


# ==== Voice room ======================================================================================================
async def vc_create_and_check():
    circle_color_emoji = ['⚫', '⚪', '🔴', '🟠', '🟡', '🟢', '🔵', '🟣']

    # wait until bot ready
    await bot.wait_until_ready()

    guild = bot.get_guild(int(guild_id))

    voice_room_id = [int(os.getenv('vc_new_voice_channel'))]
    voice_room = []
    new_voice = []
    for x in voice_room_id:
        tmp = bot.get_channel(x)
        voice_room.append(tmp)

    while not bot.is_closed():
        #remove created channel
        if len(new_voice) > 0:
            for x in new_voice:
                if (len(x.members) == 0):
                    await x.delete()
                    new_voice.remove(x)

        for x in voice_room:
            if (len(x.members) > 0):
                member = x.members[0]
                category = bot.get_channel(x.category.id)
                vc_creator = guild.get_member(member.id)

                circle_color = random.choice(circle_color_emoji)
                new_name = f"{circle_color} {vc_creator.name}"

                #creating new voice channel
                tmp = await category.create_voice_channel(new_name)
                new_voice.append(tmp)

                #member moved
                await member.move_to(tmp)

        await asyncio.sleep(1)

# ==== KBBI ============================================================================================================
@bot.command(name='kbbi')
async def kbbi(ctx, *, content):
    delay_time = 60
    sender_id = ctx.message.author.id
    
    try:
        result = json.loads(json.dumps(KBBI(content).serialisasi()))
        result = result['entri']
        #print(json.dumps(KBBI(content).serialisasi(), indent=2))
    
        embeds = []
        for entri in result:
            nama = entri.get("nama")
            makna = entri.get("makna")
            embed = discord.Embed(title=nama)
        
            j=0
            all_mk = ""
            all_c = ""
            for mk in makna:
                j += 1
                submakna = mk.get("submakna")           # list
                contoh = mk.get("contoh")                # list
        
                i=0
                all_smk = ""
                for smk in submakna:
                    i += 1
                    all_smk = all_smk + smk + "; "
                
                if len(mk.get("kelas")) == 1:
                    all_mk = all_mk + str(j) + ". (" + mk.get("kelas")[0].get("nama") + ") " + all_smk + "\n"
                elif len(mk.get("kelas")) == 2:
                    all_mk = all_mk + str(j) + ". (" + mk.get("kelas")[0].get("nama") + ") (" + mk.get("kelas")[1].get("nama") + ") " + all_smk + "\n"
                else:
                    all_mk = all_mk + str(j) + ". " + all_smk + "\n"
            
                i=0
                all_cth = ""
                if len(contoh) > 0:
                    for cth in contoh:
                        i += 1
                        all_cth = all_cth + cth + "; "
                    all_c = all_c + "(" + str(j) + ") " + all_cth + "\n"
            
            embed.add_field(name="Makna", value=all_mk, inline=False)
        
            if len(all_c) > 0:
                embed.add_field(name="Contoh", value=all_c, inline=False)
        
            embeds.append(embed)
    
    
        # send message & add reactions
        embeds_length = len(embeds)
        page = 0
        if embeds_length == 1:
            await ctx.send(embed=embeds[page])
        else:
            msg = await ctx.send(embed=embeds[page])
            msg_id = msg.id
            await msg.add_reaction(emoji='◀️')
            await msg.add_reaction(emoji='▶️')
            await msg.add_reaction(emoji='⏹️')

            while True:
                try:
                    reaction, user = await bot.wait_for('reaction_add', timeout=delay_time)
                    if reaction is None:
                        await msg.clear_reactions()
                    elif (sender_id == user.id) and (msg_id == reaction.message.id):
                        if reaction.emoji == '◀️':
                            if page > 0:
                                page -= 1
                            elif page == 0:
                                page = embeds_length-1
                            await msg.edit(embed=embeds[page])
                        elif reaction.emoji == '▶️':
                            if page < embeds_length-1:
                                page += 1
                            elif page == embeds_length-1:
                                page = 0
                            await msg.edit(embed=embeds[page])
                        elif reaction.emoji == '⏹️':
                            await msg.clear_reactions()
                            break
                        if user.id != bot.user.id:
                            await reaction.remove(user)
                except asyncio.TimeoutError:
                    await msg.clear_reactions()
                    break
    except:
        await ctx.send("Tidak Ditemukan")

# === kill & restart ===================================================================================================
@bot.command()
async def kill(ctx):
    sender = ctx.message.author.roles
    await ctx.message.delete()

    if check_dev(sender) or check_owner(sender):
        await ctx.send("Good bye world")
        await bot.close()
        sys.exit()
    else:
        await ctx.send("Don't you dare!!")

@bot.command()
async def restart(ctx):
    sender = ctx.message.author.roles
    await ctx.message.delete()

    if check_dev(sender) or check_owner(sender):
        await ctx.send("Restarting...")
        os.system("clear")
        os.execv(sys.executable, ['python3.8'] + sys.argv)
    else:
        await ctx.send("Don't you dare!!")

def check_dev(sender_roles):
    staff = int(os.getenv('r_BOT_DEV'))

    for x in sender_roles:
        role_id = x.id
        if role_id == staff:
            return True
    return False

def check_owner(sender_roles):
    if dev:
        staff = int(os.getenv('r_OWNER'))

    for x in sender_roles:
        role_id = x.id
        if role_id == staff:
            return True
    return False

# ==== tasks ===========================================================================================================
# status_manager.start()
reply_deleter.start()
bot.loop.create_task(vc_create_and_check())

# ==== run bot =========================================================================================================
bot.run(bot_token)
