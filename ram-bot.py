import os
import random
import time

import discord
import asyncio

from dotenv import load_dotenv
from discord.ext import commands, tasks
from discord.utils import get

load_dotenv()
bot_token = os.getenv('ram_bot_token')
guild_id = os.getenv('guild_id')
bot_thumbnail = os.getenv('ram_bot_thumbnail')
BOT_PREFIX = ['??']

bot = commands.Bot(command_prefix=BOT_PREFIX)
bot.remove_command('help') #???

# ==== bot login =======================================================================================================
@bot.event
async def on_ready():
    guild = bot.get_guild(int(guild_id))
#    pantsu =

    print("Ram Bot ONLINE! connected to " + guild.name + ": " + str(guild.id))

#    if :   #at least 2 pants are in the same game
#    await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.playing, name="with the pantsu squad"))
#    elif:  #__kneko is live
#    await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name="__kneko live on twitch"))
#    elif:  #Jerryfishu is live
#    await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.listening, name="Jerryfishu live on twitch"))
#    else:
    await bot.change_presence(activity=discord.Activity(type=discord.ActivityType.watching, name="over the server"))

# ==== Bot Info ========================================================================================================
@bot.command(name='ram')
async def bot_bio(ctx):
    embed = discord.Embed(title="My Bio", color=0xb6d5ec)
    embed.add_field(name="Kana", value="ラム")
    embed.add_field(name="Romaji", value="Ram")
    embed.add_field(name="Race", value="Oni")
    embed.add_field(name="Gender", value="Female")
    embed.add_field(name="Age", value="17")
    embed.add_field(name="Affiliation", value="Kingdom of Lugnica")

    # send
    await ctx.send(embed=embed)

# ==== run bot =========================================================================================================
bot.run(bot_token)